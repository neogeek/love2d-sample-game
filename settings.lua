settings = {}

local menu = nil

function settings.load()

    menu = MenuSystem:create()

    menu.font = love.graphics.newFont('fonts/mega_man_34.ttf', 40)

    menu.font:setLineHeight(50)
    menu.font:setFilter('nearest', 'nearest')

    menu:addItem(1, 'FULLSCREEN - OFF', true)
    menu:addItem(2, 'BACK TO TITLE', true)

    menu:setMenuSelectHandler(settings.handleMenuSelect)

end

function settings.handleMenuSelect(index)

    if index == 1 then

        local fullscreen, type = love.window.getFullscreen()

        if fullscreen then

            menu:updateItem(1, 'FULLSCREEN - OFF', true)

            love.window.setFullscreen(false, type)

        else

            menu:updateItem(1, 'FULLSCREEN - ON', true)

            love.window.setFullscreen(true, type)

        end

    elseif index == 2 or index == 'escape' then

        main.switchStage(1)

    end

end

function settings.update(dt)

    menu:update(dt)

end

function settings.draw()

    love.graphics.reset()

    love.graphics.setBackgroundColor(30, 30, 30)

    love.graphics.push()

    love.graphics.translate(100, 200)

    menu:draw()

    love.graphics.pop()

end