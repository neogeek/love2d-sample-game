function hexToRGBA(hex, alpha)

    local _, _, r, g, b = hex:find('#?(%x%x)(%x%x)(%x%x)')

    if alpha == nil then

        alpha = 255

    end

    return unpack({ tonumber(r, 16), tonumber(g, 16), tonumber(b, 16), alpha})

end