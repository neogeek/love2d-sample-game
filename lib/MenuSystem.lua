MenuSystem = {}
MenuSystem.__index = MenuSystem

function MenuSystem:create()

    local self = setmetatable({}, MenuSystem)

    self.items = {}
    self.itemIndexList = {}

    self.font = nil
    self.fontColor = '#ffffff'

    self.menuSelectHandler = nil

    self.selectedItem = nil

    self.activeKeyDelay = 0.25
    self.activeKeyDelayTimeout = 0

    return self

end

function MenuSystem:addItem(index, text, enabled)

    if enabled ~= true then

        enabled = false

    end

    self.items[index] = { text = text, enabled = enabled }
    self.itemIndexList[#self.itemIndexList + 1] = index

    if self.selectedItem == nil then

        self.selectedItem = index

    end

end

function MenuSystem:updateItem(index, label, enabled)

    if self.items[index] then

        if label then

            self.items[index].text = label

        end

    end

end

function MenuSystem:setMenuSelectHandler(handler)

    self.menuSelectHandler = handler

end

function MenuSystem:update(dt)

    if self.activeKeyDelayTimeout > 0 then

        self.activeKeyDelayTimeout = self.activeKeyDelayTimeout - dt

    end

    if activeKeys['up'] then

        if self.activeKeyDelayTimeout <= 0 then

            self.activeKeyDelayTimeout = self.activeKeyDelay

            self.selectedItem = self:getPreviousEnabledIndex(self.selectedItem)

        end

    elseif activeKeys['down'] then

        if self.activeKeyDelayTimeout <= 0 then

            self.activeKeyDelayTimeout = self.activeKeyDelay

            self.selectedItem = self:getNextEnabledIndex(self.selectedItem)

        end

    elseif activeKeys['return'] then

        if self.activeKeyDelayTimeout <= 0 then

            activeKeys['return'] = false

            self.activeKeyDelayTimeout = self.activeKeyDelay

            if type(self.menuSelectHandler) == 'function' then

                self.menuSelectHandler(self.selectedItem)

            end

        end

    elseif activeKeys['escape'] then

        if self.activeKeyDelayTimeout <= 0 then

            activeKeys['escape'] = false

            self.activeKeyDelayTimeout = self.activeKeyDelay

            if type(self.menuSelectHandler) == 'function' then

                self.menuSelectHandler('escape')

            end

        end

    else

        self.activeKeyDelayTimeout = 0

    end

end

function MenuSystem:getPreviousEnabledIndex(index)

    index = index - 1

    if self.items[index] == nil then

        index = self.itemIndexList[#self.itemIndexList]

    elseif self.items[index].enabled == false then

        index = self:getPreviousEnabledIndex(index)

    end

    return index

end

function MenuSystem:getNextEnabledIndex(index)

    index = index + 1

    if self.items[index] == nil then

        index = self.itemIndexList[1]

    elseif self.items[index].enabled == false then

        index = self:getNextEnabledIndex(index)

    end

    return index

end

function MenuSystem:draw()

    local screenWidth = love.graphics.getWidth()

    local lineHeight = self.font:getHeight()

    if self.font:getLineHeight() > 1 then

        lineHeight = self.font:getLineHeight()

    end

    love.graphics.setFont(self.font)

    for i, item in ipairs(self.items) do

        if i == self.selectedItem then

            love.graphics.setColor(hexToRGBA(self.fontColor, 255))

        elseif item.enabled == false then

            love.graphics.setColor(hexToRGBA(self.fontColor, 50))

        else

            love.graphics.setColor(hexToRGBA(self.fontColor, 150))

        end

        love.graphics.printf(item.text, 0, i * lineHeight, screenWidth, 'left')

    end

end