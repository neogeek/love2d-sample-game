title = {}

local menu = nil

function title.load()

    menu = MenuSystem:create()

    menu.font = love.graphics.newFont('fonts/mega_man_34.ttf', 40)

    menu.font:setLineHeight(50)
    menu.font:setFilter('nearest', 'nearest')

    menu:addItem(1, 'NEW GAME', true)
    menu:addItem(2, 'CONTINUE', false)
    menu:addItem(3, 'SETTING', true)
    menu:addItem(4, 'QUIT', true)

    menu:setMenuSelectHandler(title.handleMenuSelect)

end

function title.handleMenuSelect(index)

    if index == 1 then

        main.switchStage(3)

    elseif index == 3 then

        main.switchStage(2)

    elseif index == 4 then

        love.event.quit()

    end

end

function title.update(dt)

    menu:update(dt)

end

function title.draw()

    love.graphics.reset()

    love.graphics.setBackgroundColor(30, 30, 30)

    love.graphics.push()

    love.graphics.translate(100, 100)

    love.graphics.setFont(love.graphics.newFont('fonts/mega_man_34.ttf', 90))

    love.graphics.printf('UNTITLED\nGAME', 0, 0, screenWidth, 'left')

    love.graphics.translate(0, 400)

    menu:draw()

    love.graphics.pop()

end