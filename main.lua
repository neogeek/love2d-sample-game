require('lib/hexToRGBA')
require('lib/MenuSystem')

require('title')
require('settings')
require('level')

main = {}

screenWidth = love.graphics.getWidth()
screenHeight = love.graphics.getHeight()

stage = { [1] = title, [2] = settings, [3] = level }

activeStage = nil

activeKeys = {

    ['up'] = false,
    ['down'] = false,
    ['left'] = false,
    ['right'] = false,
    ['return'] = false,
    ['escape'] = false

}

joystickButtonMapping = {

    [1] = 'return',
    [9] = 'escape',
    [12] = 'up',
    [13] = 'down',
    [14] = 'left',
    [15] = 'right'

}

joysticks = nil

function love.load()

    joysticks = love.joystick.getJoysticks()

    main.switchStage(1)

end

function love.update(dt)

    if stage[activeStage] and stage[activeStage].update then

        stage[activeStage].update(dt)

    end

end

function love.draw()

    love.graphics.reset()

    if stage[activeStage] and stage[activeStage].draw then

        stage[activeStage].draw()

    end

end

function love.keypressed(key, rep)

    print('Key pressed = ' .. key)

    if stage[activeStage] and stage[activeStage].keypressed then

        stage[activeStage].keypressed(key, rep)

    end

    activeKeys[key] = true

end

function love.keyreleased(key)

    if stage[activeStage] and stage[activeStage].keyreleased then

        stage[activeStage].keyreleased(key)

    end

    activeKeys[key] = false

end

function love.joystickpressed(joystick, button)

    print('Joystick button pressed = ' .. button)

    if joystickButtonMapping[button] then

        activeKeys[joystickButtonMapping[button]] = true

        if stage[activeStage] and stage[activeStage].keypressed then

            stage[activeStage].keypressed(button)

        end

    end

end

function love.joystickreleased(joystick, button)

    if joystickButtonMapping[button] then

        activeKeys[joystickButtonMapping[button]] = false

        if stage[activeStage] and stage[activeStage].keyreleased then

            stage[activeStage].keyreleased(button)

        end

    end

end

function main.switchStage(num)

    if activeStage ~= num then

        activeStage = num

        if stage[activeStage].load then

            stage[activeStage].load()

        end

    end

end