level = {}

local data = {}

local width = 50
local height = 50

function level.load()

    data = level.loadLevel('maps/level1.txt')

    -- print(level.saveLevel('custom-map-level1.txt', data))

end

function level.update(dt)


end

function level.draw()

    if #data then

        for r, row in ipairs(data) do

            for c, col in ipairs(row) do

                love.graphics.reset()

                if col == 'w' then

                    love.graphics.setColor(hexToRGBA('#ff0000'))

                    love.graphics.rectangle('fill', (c -1) * width, (r -1) * height, width, height)

                elseif col == 'b' then

                    love.graphics.setColor(hexToRGBA('#ff00ff'))

                    love.graphics.rectangle('fill', (c -1) * width, (r -1) * height, width, height)

                elseif col == 'p' then

                    love.graphics.setColor(hexToRGBA('#ffffff'))

                    love.graphics.rectangle('fill', (c -1) * width, (r -1) * height, width, height)

                end

            end

        end

    end

end

function level.loadLevel(filename)

    local content = love.filesystem.read(filename)
    local data = {}

    for row in string.gmatch(content, '([a-z]+)\n?') do

        local temp = {}

        for col in string.gmatch(row, '[a-z]') do

            temp[#temp + 1] = col

        end

        data[#data + 1] = temp

    end

    return data

end

function level.saveLevel(filename, data)

    local content = ''

    for _, row in ipairs(data) do

        for _, col in ipairs(row) do

            content = content .. col

        end

        content = content .. '\n'

    end

    return love.filesystem.write(filename, content)

end